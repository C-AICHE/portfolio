import { Helmet } from 'react-helmet-async';
import styles from './Projet2.module.css';
import Page1 from '../resources/App_bureau/Page1.png';
import Page2 from '../resources/App_bureau/Page2.png';
import Page3 from '../resources/App_bureau/Page3.png';


export default function Projet2() {
    return <div className={styles.app} >
        <Helmet>
            <title>Projet2 - Portfolio</title>
            <meta name="description" content="La page Projet2 contient des informations et des photos d'un logiciel de bureau réalisé par Choukri. Ce logiciel a faire la géstion des stagiaires d'un collège selon le programme " />
        </Helmet>
        <section className={styles.titre}>
            <h1>Application Bureau</h1>
        </section>
        <div className={styles.intro}>
            <p>
                Dans le cadre d'un projet d'études,
                On devait développer une application permetant de de gérer les
                stagiaires de La Cité collégiale dans tous les programmes.
            </p>
        </div>

        <section className={styles.partie1}>
            <div className={styles.contenu} >
                <img className={styles.img} src={Page2} alt="Section des programmes" />
                <div>
                    <p>
                        L'application comporte trois sections (Programmes, Stagiaires, Consulter)
                    </p>
                    <p>
                        La section programmes permet de gérer les informations des programmes.
                    </p>
                </div>
            </div>
            <div className={styles.contenu}>
                <div>
                    <p>
                        La dexième section c'est la section des stagiares, elle permet de gérer les stagiares de tous les programmes
                    </p>
                </div>
                <img className={styles.img} src={Page1} alt="section des stagiaires" />

            </div>
            <div className={styles.contenu}>
                <img className={styles.img} src={Page3} alt="Section consulter" />
                <div>
                    <p>
                        Pour voir les stagiaires de chaque programme, il suffit de séléctionner le programme voulu dans le menu déroulant puis cliquer sur rechercher
                    </p>
                </div>
            </div>
        </section>
        <div className={styles.partie2}>
            <p>L'application est réalisée avec les langages  C# et SQL </p>
        </div>
    </div>
}
