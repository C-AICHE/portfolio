import { Helmet } from 'react-helmet-async';
import styles from './Accueil.module.css';
import Moi from '../resources/Moi2.jpg';


export default function Accueil () {
    return <div className={styles.accueil}>
        <Helmet>
            <title>Accueil - Portfolio</title>
            <meta name="description" content="Page d'accueil du portfolio de aiche choukri, elle contient une petite présentation de Choukri Aiche plus ces competences en programmation informatique " />
        </Helmet>
        <section className={styles.background}>
            <div className={styles.contnent}>
                <div>
                    <h1>Choukri Aiche</h1>
                    <hr />
                    <p>Dévelopeur Informatique</p>
                </div>
            </div>
        </section>
        <section className={styles.background2}>
            <div className={styles.titre}>
                <h2>Présentation</h2>
            </div>
            <div className={styles.description}>
                <img className={styles.img} src={Moi} alt="choukri" />
                <div >
                    <p>Allo! je m'appelle Choukri Aiche, j'ai 25 ans </p>
                    <p>Aprés plusieurs années d'études, l'obtention de mon diplome et quelques expériences professionnelles en médecine vétérinaire, Je me suis redirigé vers le domaine de la programmation</p>
                    <p>Je suis actuellement une formation en Programmation informatique à LA CITÉ collégiale, Désormais je suis apte à concevoir des applications informatiques avec différents langages de programmation</p>
                </div>
            </div>

        </section>
        <section className={styles.background3} >
            <div className={styles.titre}>
                <h2>Mes Compétences</h2>
            </div>
            <div className={styles.competences}>
                <div className={styles.box}>
                    <h3>Langages Web</h3>
                    <div>
                        <ul >
                            <li>HTML</li>
                            <li>CSS</li>
                            <li>JavaScript</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.box}>
                    <h3>Langages</h3>
                    <div>
                        <ul >
                            <li>C#</li>
                            <li>Java</li>
                            <li>WPF</li>
                            <li>SQL</li>
                        </ul>
                    </div>
                </div>
                <div className={styles.box}>
                    <h3>Frameworks et Librairies</h3>
                    <div>
                        <ul >
                            <li>Bootstrap</li>
                            <li>React</li>
                        </ul>
                    </div>
                </div>
                <div  className={styles.box}>
                    <h3>Outils et Logiciels</h3>
                    <div>
                        <ul >
                            <li>Visuel studio</li>
                            <li>Visuel studio code</li>
                            <li>Xcode</li>
                            <li>Android Studio</li>
                            <li>GitLab</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
}