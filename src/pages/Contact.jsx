import { Helmet } from 'react-helmet-async';
import IconCall from '../components/Icon_call.jsx';
import Formulaire from '../components/Formulaire.jsx';
import styles from './Contact.module.css';

export default function Contact() {
    return <section className={styles.mesure}>
        <Helmet>
            <title>Contact - Portfolio</title>
            <meta name="description" content="Page contact contient les coordonnées de Choukri Aiche et un formulaire pour lui envoyer un email " />
        </Helmet>
        <div className={styles.contenu} >

            <div className={styles.icon}>
                <strong>Téléphone<IconCall /></strong>
                <a href="tel:+1 514-817-7881"> 514-817-7881</a>
            </div>
            <div className={styles.icon}>
                <strong>Adresse</strong>
                <address>274 bou Riel, Gatineau,QC </address>
            </div>
            <div >
                <Formulaire />
            </div>
        </div>

    </section>
    /* <div className={styles.icon}>
        <strong >Adresse e-mail<IconContact /></strong>
        <a href="mailto:choukriaiche@gmail.com">Choukriaiche@gmail.com</a>
    </div> */
}