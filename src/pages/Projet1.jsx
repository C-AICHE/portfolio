import { Helmet } from 'react-helmet-async';
import styles from './Projet1.module.css';
import Usa1 from '../resources/App_mobile/Usa1.png';
import Usa2 from '../resources/App_mobile/Usa2.png';
import Usa3 from '../resources/App_mobile/Usa3.png';




export default function Projet1() {
    return <div >
        <Helmet>
            <title>Projet1 - Portfolio</title>
            <meta name="description" content="Page projet1 un contient des informations et des photos d'une application réalisée par Choukri. Dans cette application android on trouve des informations sur chaque état des USA" />
        </Helmet>
        <section className={styles.titre}>
            <h1>Application Mobile</h1>
        </section>

        <section className={styles.partie1}>
            <div className={styles.contenu} >
                <img className={styles.img} src={Usa1} alt="Liste des etats des usa" />
                <div>
                    <p>
                        Dans le cadre d'un projet d'études,
                        On devait réaliser une application mobile
                        contenant les informations de tous les états des USA
                    </p>
                </div>
            </div>
            <div  className={styles.contenu}>
                <div>
                    <p>
                        L'application contient une liste de tous les états des USA.
                    </p>
                    <p>
                        En selectionnant un état , ses informations  apparaissent en haut de l'application.
                    </p>
                </div>
                <img className={styles.img2} src={Usa2} alt="Liste des etats des usa" />

            </div>
            <div className={styles.contenu}>
                <div>
                    <p>
                        Le boutton WIKI ouvre une page "WIKIPÉDIA" sur le navigateur pour
                        plus d'informations concernant l'ètat chosi.
                    </p>
                </div>
                <img className={styles.img} src={Usa3} alt="Lien vers wiiki peddia" />

            </div>
        </section>

        <section className={styles.partie2}>
            <p>
                L'application est réalisée avec android studio en language Java.
            </p>
        </section>
    </div>
}