import { Outlet } from "react-router-dom";
import { Suspense } from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";

export default function Layout() {
    return <>
        <Header />
        <Suspense fallback={<>...</>}>
            <Outlet />
        </Suspense>

        <Footer />
    </>
}