import emailValidator from 'email-validator';
import { useState } from 'react';

import styles from './Formulaire.module.css';

export default function Formulaire() {
    const [courrielClient, setCourrielClient] = useState('');
    const [courrielTouched, setCourrielTouched] = useState(false);
    const handleCourrielChange = (event) => {
        setCourrielClient(event.currentTarget.value);
        setCourrielTouched(true);
    }

    const [messageClient, setMessageClient] = useState('');
    const [messageTouched, setMessageTouched] = useState(false);
    const handleMessageChange = (event) => {
        setMessageClient(event.currentTarget.value);
        setMessageTouched(true);
    }


    const handleSubmit = (event) => {
        event.preventDefault();

        if (courrielClient !== '' && emailValidator.validate(courrielClient) && messageClient !== '') {
            console.log("e-mail du client :"+courrielClient);
            console.log("Le message du client :"+messageClient);

            setCourrielClient('');
            setCourrielTouched(false);
            setMessageClient('');
            setMessageTouched(false);
        }
    }

    return <form noValidate className={styles.form} onSubmit={handleSubmit}>
        <label>
            <strong>Courriel</strong>
            <input placeholder='Entrer votre adresse courriel' type="email" value={courrielClient} onChange={handleCourrielChange} />
        </label>
        {courrielTouched && courrielClient === '' &&
            <div className={styles.erreur}>
                L'adresse courriel n'est pas spécifié.
            </div>
        }
        {courrielTouched && !emailValidator.validate(courrielClient) &&
            <div className={styles.erreur}>
                L'adresse courriel n'est pas valide.
            </div>
        }

        <label>
            <strong>Message</strong>
            <textarea placeholder='Entrer votre message' value={messageClient} onChange={handleMessageChange} />
        </label>
        {messageTouched && messageClient === '' &&
            <div className={styles.erreur}>
                Message non spécifié!
            </div>
        }

        <input type="submit" />
    </form>
}