import CA from '../resources/CA.png';
import styles from './Footer.module.css';


export default function Footer(){
    return <footer className={styles.footer}>
        <div className={styles.links}>
            <img src={CA} alt="LOGO CA" />
        </div>
        <p className={styles.cpopyright}>&copy;2022 - Portfolio de Choukri Aiche</p>
    </footer>
}