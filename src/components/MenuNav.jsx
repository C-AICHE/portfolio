import styles from './MenuNav.module.css';
import IconAccueil from './Icon_accueil';
import IconProjet from './Icon_projet';
import IconContact from './Icon_contact';
import {NavLink } from 'react-router-dom';


export default function MenuNav() {
    return <nav>
        <ul className={styles.list}>
            <li >
                <NavLink to="/" className={({isActive}) => isActive ? styles.actif : styles.notactif}>
                    <IconAccueil />Accueil
                </NavLink>
            </li>
            <li>
                <NavLink to="/projet1" className={({isActive}) => isActive ? styles.actif : styles.notactif}>
                    <IconProjet />Projet1
                </NavLink>
            </li>
            <li>
                <NavLink to="/projet2" className={({isActive}) => isActive ? styles.actif : styles.notactif}>
                    <IconProjet />Projet2
                </NavLink>
            </li>
            <li>
                <NavLink to="/contact" className={({isActive}) => isActive ? styles.actif : styles.notactif}>
                    <IconContact />Contact
                </NavLink>
            </li>
        </ul>
    </nav>
}