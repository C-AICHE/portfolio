import styles from './Logo.module.css';
import CA from '../resources/CA.png';


export default function Logo() {
    return <div className={ styles.logo }>
        <div >
            <img className={styles.img} src={CA} alt="" />
            <div>Portfolio</div>
        </div>
    </div>
}
