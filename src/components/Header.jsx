import MenuNav from "./MenuNav";
import Logo from "./Logo";
import styles from './Header.module.css';


export default function Header(){
    return <header className={styles.header}>
        <div>
            <Logo/>
        </div>
        <div className={styles.menu}>
            <MenuNav/>
        </div>
        
    </header>
}